package authorization

import (
	"crypto/rsa"
	"github.com/golang-jwt/jwt/v5"
	"io/ioutil"
	"sync"
)

// Variables para almacenar las llaves
var (
	singKey   *rsa.PrivateKey // llave privada para firmar
	verifyKet *rsa.PublicKey  // llave publica para verificar
	once      sync.Once       //solo se ejecuta una vez
)

// Funcion para cargar los archivos de las llaves que se ejecuta una vez
func LoadFiles(privateFile, publicFile string) error { //es exportable porque empieza con mayuscula
	var err error
	once.Do(func() {
		err = loadFiles(privateFile, publicFile) // si ubo error se regresa si no se regresa nil
	})
	return err
}

// esto me lee los archivos y los carga
func loadFiles(privateFile, publicFile string) error {

	// Cargar llave privada
	privateBytes, err := ioutil.ReadFile(privateFile) //con readfile se lee el archivo y cierra el archivo
	if err != nil {
		return err
	}

	publicBytes, err := ioutil.ReadFile(publicFile)
	if err != nil {
		return err
	}
	return parseRSA(privateBytes, publicBytes) //el error que pasa en esta funcion se regresa
}

// esto me parsea las llaves
func parseRSA(privateBytes, publicBytes []byte) error { //le pongo bytes porque es lo que se lee del archivo

	var err error
	singKey, err = jwt.ParseRSAPrivateKeyFromPEM(privateBytes) //parsea la llave privada
	if err != nil {
		return err
	}

	verifyKet, err = jwt.ParseRSAPublicKeyFromPEM(publicBytes) //parsea la llave publica
	if err != nil {
		return err
	}

	return nil // si no hay error regresa nil
}
