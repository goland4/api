package authorization

import (
	"apis/Crud/model"
	"errors"
	"github.com/golang-jwt/jwt/v5"
	"time"
)

func GenerateToken(data *model.Login) (string, error) {

	//el claim es un token que se le asigna a un usuario para que pueda acceder a los recursos de la API
	claim := model.Claims{
		Email: data.Email,
		RegisteredClaims: jwt.RegisteredClaims{ //se le asignan los valores al claim que es la estructura del token
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(time.Hour * 2)),
			Issuer:    "API",
		},
	}

	//se crea el token
	token := jwt.NewWithClaims(jwt.SigningMethodRS256, claim)
	//se firma el token
	signedToken, err := token.SignedString(singKey) //se firma el token con la llave privada
	if err != nil {
		return "", err
	}
	return signedToken, nil
}

func ValidateToken(t string) (model.Claims, error) {
	token, err := jwt.ParseWithClaims(t, &model.Claims{}, verifyFunction) //se valida el token
	if err != nil {
		return model.Claims{}, err
	}
	if !token.Valid { //si el token no es valido
		return model.Claims{}, errors.New("token invalido")
	}
	claims, ok := token.Claims.(*model.Claims) // del token se extraen los claims
	if !ok {
		return model.Claims{}, errors.New("no se pudo obtener los claims")
	}

	return *claims, nil //se regresan los claims si no hay error
}

func verifyFunction(t *jwt.Token) (interface{}, error) {
	return verifyKet, nil //esto retorna la llave publica para verificar el token
}
