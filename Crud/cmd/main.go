package main

import (
	"apis/Crud/authorization"
	"apis/Crud/handler"
	"apis/Crud/storage"
	"log"
	"net/http"
)

func main() {
	err := authorization.LoadFiles("./Crud/cmd/certificates/app.rsa", "./Crud/cmd/certificates/app.rsa.pub")
	if err != nil {
		log.Fatalf("Error al cargar los archivos: %v", err)
	}

	store := storage.NewMemory()
	mux := http.NewServeMux() //el mux es un enrutador HTTP que se basa en la URL de la petición para redirigir la petición al controlador correspondiente.

	handler.RoutePerson(mux, &store)
	handler.RouteLogin(mux, &store)

	log.Println("Server running on port 8080")
	err = http.ListenAndServe(":8080", mux)
	if err != nil {
		log.Printf("Error: %v", err)
	}

}
