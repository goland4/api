package handler

import (
	"apis/Crud/authorization"
	"apis/Crud/model"
	"encoding/json"
	"net/http"
)

type login struct {
	storage Storage
}

func newLogin(s Storage) *login {
	return &login{s} //le envio un {s} que es el storage ya que el storage es una interfaz
}

func (l *login) login(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		response := newResponse(Error, "Método no permitido", nil)
		responseJSON(w, http.StatusMethodNotAllowed, response)
		return
	}

	data := model.Login{}
	err := json.NewDecoder(r.Body).Decode(&data) //decodificamos el json que nos llega en el body
	if err != nil {
		response := newResponse(Error, "estructura incorrecta", nil)
		responseJSON(w, http.StatusBadRequest, response)
		return
	}

	if !isLoginValid(&data) {
		response := newResponse(Error, "credenciales invalidas", nil)
		responseJSON(w, http.StatusUnauthorized, response)
		return
	}

	token, err := authorization.GenerateToken(&data)
	if err != nil {
		response := newResponse(Error, "no se pudo generar el token", nil)
		responseJSON(w, http.StatusInternalServerError, response)
		return
	}

	dataToken := map[string]string{"token": token}
	response := newResponse(Message, "token generado", dataToken)
	responseJSON(w, http.StatusOK, response)

}

func isLoginValid(data *model.Login) bool {
	return data.Email == "colocolo@colocolo.cl" && data.Password == "colocolo"
}
