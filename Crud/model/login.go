package model

import "github.com/golang-jwt/jwt/v5"

type Login struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

// el claim es un token que se le asigna a un usuario para que pueda acceder a los recursos de la API
type Claims struct { //esta es la estructura del token
	Email string `json:"email"`
	jwt.RegisteredClaims
}
